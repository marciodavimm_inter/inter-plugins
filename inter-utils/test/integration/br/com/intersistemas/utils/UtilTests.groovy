package br.com.intersistemas.utils

/**
 * Created by MarcioDavi on 06/03/2017.
 */
class UtilTests extends GroovyTestCase {
    void testIsUrlValid(){
        log.info "iniciando testes..."
        def url = "http://www.intersistemas.com.br"
        def isValid = Util.isUrlValid(url)
        assertEquals true, isValid
    }
}
