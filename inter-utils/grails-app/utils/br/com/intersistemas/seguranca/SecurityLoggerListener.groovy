package br.com.intersistemas.seguranca

import br.com.intersistemas.utils.Util
import grails.converters.JSON
import grails.util.Holders

import javax.servlet.http.*
import org.apache.commons.logging.LogFactory
import org.springframework.context.ApplicationListener
import org.springframework.security.authentication.event.AbstractAuthenticationEvent
import org.springframework.security.core.Authentication
import org.springframework.security.web.authentication.logout.LogoutHandler

/**
 * Created by MarcioDavi on 16/12/2016.
 */
class SecurityLoggerListener implements
        ApplicationListener<AbstractAuthenticationEvent>, LogoutHandler {

    private static final log = LogFactory.getLog(this)

    void onApplicationEvent(AbstractAuthenticationEvent event) {
        event?.authentication?.with {
            def hoje = new Date()
            def username = principal?.hasProperty('username')?.getProperty(principal) ?: principal
            def jsonData = [
                    data: hoje,
                    time: hoje.time,
                    username: username?:"### username não definido ###",
                    evento: event?.class?.simpleName,
                    remoteAddress: details?.remoteAddress,
                    sessionId: details?.sessionId
            ]

            def str = "-".padLeft(100,"-") + "\njson: ${jsonData as JSON}\n"
            addLog(str)
        }
    }

    void logout(HttpServletRequest request, HttpServletResponse response, Authentication authentication) {
        try {
            authentication?.with {
                def username = principal?.hasProperty('username')?.getProperty(principal) ?: principal
                def jsonData = [
                        data         : new Date(),
                        username     : username ?: "### username não definido ###",
                        evento       : "Logout(estático)",
                        remoteAddress: details?.remoteAddress,
                        sessionId    : details?.sessionId
                ]

                def str = "-".padLeft(100, "-") + "\njson: ${jsonData as JSON}\n"
                addLog(str)
            }
        }
        catch (e){
            Util.printExcecaoInter(e, log)
        }
    }

    private static addLog(String texto){
        Util.printHeader(texto, log)

        def dateFilePath = Util.getDateFilePath(Holders.config.clubedetiro.log.filepath.securityEvents, new Date())
        if (dateFilePath) {
            File file = new File(dateFilePath)
//                  se arquivo não existe, cria.
            file?.append(texto)
        }
        else {
            Util.printHeader("caminho de arquivo inválido\nbase: ${Holders.config.clubedetiro.log.filepath.securityEvents}\ndateFilePath: ${dateFilePath}", log)
        }
    }

}


