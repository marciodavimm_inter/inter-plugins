package br.com.intersistemas.utils

import java.security.SecureRandom

/**
 * Created by MarcioDavi on 18/07/2016.
 */
final class SessionIdentifierGenerator {
    private SecureRandom random = new SecureRandom();

    public String nextSessionId() {
        return new BigInteger(130, random).toString(32);
    }
}

