package br.com.intersistemas.utils

import grails.util.Environment
import grails.util.Holders
import groovy.json.JsonSlurper
import org.apache.commons.lang.RandomStringUtils
import org.apache.commons.lang.WordUtils
import org.apache.commons.logging.Log
import org.apache.commons.validator.routines.EmailValidator
import org.codehaus.groovy.grails.validation.routines.InetAddressValidator
import org.codehaus.groovy.grails.validation.routines.UrlValidator
import org.springframework.context.MessageSource
import org.springframework.validation.Errors


/**
 * Created by suporte on 10/09/2015.
 */
class Util {
	static SessionIdentifierGenerator sessionIdentifierGenerator = new SessionIdentifierGenerator()

    static List tratarExcecao(Exception e) {
        def listaItensStr = []
        try {
            listaItensStr << "EXCECAO - ${new Date().format('yyyy-MM-dd HH:mm:ss')}: " + e?.localizedMessage
            listaItensStr << e?.getStackTrace()?.findAll { it?.getClassName()?.contains("br.com.intersistemas") }
        } catch (e2) {
            e2.printStackTrace()
        }
        listaItensStr?.flatten()
    }

    /**
     * imprime no printStream informado o stacktrace da exception dada, filtrado por classes no pacote da intersistemas
     * @param e
     * @param log
     */
    static void printExcecaoInterToStream(Exception e, PrintStream out = System.out) {
        def listaItensStr = tratarExcecao(e)
        out.println getPrintHeader( listaItensStr.join('\n') )
    }

    /**
     * imprime no log informado o stacktrace da exception dada, filtrado por classes no pacote da intersistemas
     * @param e
     * @param log
     */
    static void printExcecaoInter(Exception e, Log log) {
        def listaItensStr = tratarExcecao(e)
        log.info getPrintHeader( listaItensStr?.join('\n') )
    }

    static void printHeader(String text, Log log){
        printHeader(text, log, 100, '=')
    }

    static void printHeader(String text, Log log, Integer size, String character){
        log.info getPrintHeader(text, size, character)
    }

    /**
     * Retorna uma string formatada para impressao no console/log , com barra superior e ambiente atual destacado
     * @param str
     * @param size
     * @param character
     * @return
     */
    static String getPrintHeaderWithEnvironment( String str=" # NADA A EXIBIR # " , Integer size=100, String character='='){
        size = size?: 100
        character = character?: '='

        def env = Environment?.current?.toString()?.center(size,character)
        return '\n' + env + getPrintHeader(str, size, character) + '\n'
    }

    static String getPrintHeader( String str=" # NADA A EXIBIR # " , Integer size=100, String character='='){
        size = size?: 100
        character = character?: '='

        def barra = character?.padLeft(size,character)
        def text = str?.toString()?.center(size, ' ')
        return "\n" + barra + "\n" + text + "\n" + barra + "\n"
    }

    static String pegarErros(Errors errors) {
        String erros = ""
        try {
//            def errosTmp = getPrintErrors(errors)?.join('\n')
            errors?.each {
                erros += "Campo com erro no modelo '${it?.fieldError?.getObjectName()}' no campo ${it?.fieldError?.field}: rejeitou o valor [${it?.fieldError?.rejectedValue}]; "
            }
        } catch (e2) {
            printExcecaoInterToStream(e2)
        }
        return erros
    }

    static List getPrintErrors(Errors errors){
        MessageSource messageSource = Holders.grailsApplication.mainContext.getBean('messageSource')
        def erros = getPrintErrors(errors, messageSource)
        erros
    }

    static List getPrintErrors(Errors errors, MessageSource messageSource){
        List erros = errors?.allErrors?.collect({ messageSource?.getMessage( it, Locale.default ) }) ?: []
        erros
    }

    static List getPrintErrors(domainInstance){
        MessageSource messageSource = Holders.grailsApplication.mainContext.getBean('messageSource')
        def erros = getPrintErrors(domainInstance, messageSource)
        erros
    }

    static List getPrintErrors(domainInstance, MessageSource messageSource){
        List erros = domainInstance?.errors?.allErrors?.collect({ messageSource?.getMessage( it, Locale.default ) /*it*/ }) ?: []
        erros
    }

	static String generateRandomSessionIdentifier(){
		sessionIdentifierGenerator.nextSessionId();
	}

	/**
	 * ex.: uuid = 2d7428a6-b58c-4008-8575-f05549f16316
	 * @return
	 */
	static String generateRandomUUID (){
		UUID.randomUUID().toString();
	}

	static String generateRandomString(Integer comprimento=32){
		String charset = (('a'..'z') + ('A'..'Z') + ('0'..'9')).join('')
		RandomStringUtils.random(comprimento, charset.toCharArray())
	}

	static Boolean isEmail(String email=""){
		EmailValidator.instance.isValid(email)
	}

	static String getUserPartFromEmail(String email){
//		email = email?.decodeHTML()
//		if( !isEmail(email)) return ""
		email?.take( email?.indexOf("@") )?.toString()
	}

	static Boolean isCpfCnpjBasedEmail(String email){
//		if( !isEmail(email) ) return false
		getUserPartFromEmail(email) ==~ /(\D*)(\d{11,14})/
	}

	/**
	 * recebe uma string de url e devolve o host, incluindo 'www.'.
	 * @param url
	 * @return
	 */
	static String getHostFromUrl(String url){
		URI uri = new URI(url)
		uri.getHost()
	}

	static Boolean isIpUrl(String url){
		def isValidIp = InetAddressValidator.getInstance().isValid(url)
		isValidIp
	}

	/**
	 * recebe uma string (ex.: url ou request.serverName) e retorna o host SEM 'www.'
	 * @param url
	 * @return
	 */
	static String getHostSemWWW(String url){
		def host = getHostFromUrl(url)
//		host?.replaceAll(/^(w{3}\d?\.?)/ , '')
//		url?.replaceAll(/^(http(s)?:\/\/)?(w{3}\d?\.)?/ , '')
		host?.replaceAll(/^(http(s)?:\/\/)?(w{3}\d?\.)?/ , '')
	}

	static Boolean isUrlValid(String url){
		UrlValidator.instance.isValid(url)
	}

	static Date getPrimeiroDiaDoMesAno(Date data){
		Calendar cal = Calendar.getInstance();
		cal.setTime(data);
		cal.set(Calendar.DATE, 1);
		return cal.getTime();
	}

	static Date getUltimoDiaDoMesAno(Date data){
		Calendar cal = Calendar.getInstance();
		cal.setTime(data);
		cal.set(Calendar.DATE, cal.getActualMaximum(Calendar.DATE));
		return cal.getTime();
	}

	static String unformatNumber(String pNum) {
		return pNum.replaceAll("[./-]", "")
	}

	static String formatNumberInputString(String num){
		num?.replace('.','')?.replace(',','.')
	}

	public static Boolean isAmbienteProducao(){
		Environment.current == Environment.PRODUCTION
	}

	/**
	 * caminho de arquivos em sistemas windows: troca todas as '/' por '\' na string.
	 * Não altera string original.
	 * @param filePath
	 * @return a string após substituições, se houvererm
	 */
	public static String corrigirFilePathParaWindows(String filePath){
		filePath.replace("/", "\\")
	}

    /**
     * recebe um caminho de arquivo e uma data.
     * retorna o caminho de arquivo com a data no nome do arquivo final. ex: /caminho/para/arquivo/[2017-01-31]fileName.txt
     * @param filePath
     * @param data
     * @return
     */
    public static String getDateFilePath(String filePath, Date data){
        if(!filePath) return null
        if(!data) data = new Date()
        def base = filePath?.find(/[\/\\]?\w*(\.\w{3}){0,2}$/)          // \w = word char = [a-zA-Z_0-9]
        def fullFileName = base.replaceAll(/^[\/\\]/,"")

//        alternativa
//        def file = new File(filePath)
//        fullFileName = file.name

        def partes = fullFileName.split(/\./,2)
        def nome = partes?.first() ?:""
        def extensao = partes?.last() ?:""

        def dateFileName = "${nome}[${data?.format("yyyy-MM-dd")}].${extensao}"
        def result = filePath.replace(fullFileName,dateFileName)
        result
    }


	/**
	 * caminho de arquivos em sistemas unix: troca todas as '\' por '/' na string.
	 * Não altera string original.
	 * @param filePath
	 * @return a string após substituições, se houverem
	 */
	public static String corrigirFilePathParaUnix(String filePath){
		filePath.replace("\\", "/")
	}

    public static Map jsonStringToMap(String jsonStr){
        JsonSlurper slurper = new JsonSlurper()
        def dados = slurper.parseText(jsonStr)
        dados as Map
    }

	private static Boolean isCpf(String cpf) {
		if ((cpf.length() != 11) ||
				(cpf == "00000000000") || (cpf == "11111111111") ||
				(cpf == "22222222222") || (cpf == "33333333333") ||
				(cpf == "44444444444") || (cpf == "55555555555") ||
				(cpf == "66666666666") || (cpf == "77777777777") ||
				(cpf == "88888888888") || (cpf == "99999999999")) {
			return false;
		}

		int soma = 0
		Double resto
		int i

		for (i = 1; i <= 9; i++) {
			soma += Math.floor(cpf[i - 1] as int) * (11 - i)
		}

		resto = 11 - (soma - (Math.floor(soma / 11) * 11))

		if ((resto == 10) || (resto == 11)) {
			resto = 0;
		}

		if (resto != Math.floor(cpf[9] as int)) {
			return false;
		}

		soma = 0

		for (i = 1; i <= 10; i++) {
			soma += (cpf[i - 1] as int) * (12 - i)
		}

		resto = 11 - (soma - (Math.floor(soma / 11) * 11))

		if ((resto == 10) || (resto == 11)) {
			resto = 0
		}

		if (resto != Math.floor(cpf[10] as int)) {
			return false
		}

		return true
	}

	private static Boolean isCnpj(String s) {
		int i
		String c = s.substring(0, 12)
		String dv = s.substring(12)
		int d1 = 0;

		for (i = 0; i < 12; i++) {
			d1 += (c[11 - i] as int) * (2 + (i % 8))
		}

		if (d1 == 0) return false

		d1 = 11 - (d1 % 11)

		if (d1 > 9) d1 = 0

		if (dv[0] as int != d1) {
			return false
		}

		d1 *= 2

		for (i = 0; i < 12; i++) {
			d1 += (c[11 - i] as int) * (2 + ((i + 1) % 8))
		}

		d1 = 11 - (d1 % 11)

		if (d1 > 9) d1 = 0

		if (dv[1] as int != d1) {
			return false
		}

		return true
	}

	static Boolean isCpfCnpj(String valor) {
		boolean retorno = false
		String numero = valor

		numero = unformatNumber(numero)

		if (numero == "") return true

		if (numero.length() > 11) {
			if (isCnpj(numero)) {
				retorno = true;
			}
		} else {
			if (isCpf(numero)) {
				retorno = true
			}
		}
		return retorno;
	}

	static Map contentTypeImages = [
			bmp : "image/bmp",
			cod : "image/cis-cod",
			gif : "image/gif",
			ief : "image/ief",
			jpe : "image/jpeg",
			jpeg: "image/jpeg",
			jpg : "image/jpeg",
			jfif: "image/pipeg",
			svg : "image/svg+xml",
			tif : "image/tiff",
			tiff: "image/tiff",
			ras : "image/x-cmu-raster",
			cmx : "image/x-cmx",
			ico : "image/x-icon",
			pnm : "image/x-portable-anymap",
			png : "image/png",
			pbm : "image/x-portable-bitmap",
			pgm : "image/x-portable-graymap",
			ppm : "image/x-portable-pixmap",
			rgb : "image/x-rgb",
			xbm : "image/x-xbitmap",
			xpm : "image/x-xpixmap",
			xwd : "image/x-xwindowdump"
	]

	static int getDiferencaMes(Date dataInicio, Date dataFim) {
		int count = 0
		if ((dataInicio != null) && (dataFim != null) && dataInicio.before(dataFim)) {
			Calendar inicio = Calendar.getInstance()
			inicio.setTime(dataInicio)
			Calendar fim = Calendar.getInstance()
			fim.setTime(dataFim)
			while ((inicio.get(Calendar.MONTH) != fim.get(Calendar.MONTH)) || (inicio.get(Calendar.YEAR) != fim.get(Calendar.YEAR))) {
				inicio.add(Calendar.MONTH, 1)
				count++
			}
		}
		return count
	}

	static BigDecimal convert2casas(BigDecimal num) {
		int decimalPlace = 2
		BigDecimal bd = new BigDecimal(num)
		bd = bd.setScale(decimalPlace, BigDecimal.ROUND_HALF_UP)
		return bd
	}

	static String stringToCamelCaseFully(String texto, String delimitadores=' '){
//		"".tokenize(' ')*.toLowerCase()*.capitalize().join()
		WordUtils.capitalizeFully(texto, delimitadores?.toCharArray())
	}

	static String stringToCamelCase(String texto, String delimitadores=' '){
		WordUtils.capitalize(texto, delimitadores.toCharArray())
	}

	static boolean validateFile(File file, String extensao) {
		String fileName = file.getName();
		String[] ext = fileName.split("\\.");
		int i = ext.length;
		return extensao?.equalsIgnoreCase(ext[i - 1])
	}


}

