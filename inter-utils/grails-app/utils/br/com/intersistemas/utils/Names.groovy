package br.com.intersistemas.utils

/**
 * Created by suporte on 22/12/2015.
 */
class Names {

	static midiaMimeTypes = [
	        'Imagem': 'image/*',
			'Documento': 'application/*,text/*',
	        'Arquivo': 'application/*,text/*,image/*,audio/*,video/*',
	        'Audio': 'audio/*',
	        'Video': 'video/*',
	        'Url': '',
	        'Principal': 'image/jpeg,image/png',
	]

	static midiaExtensoes = [
	        'Imagem': 'png,jpg,jpeg,bmp,gif',
	        'Arquivo': '*',
	        'Audio': 'wma,mp3,mp4,wav,wave',
	        'Video': 'avi,wmv,mpg,mpeg,mp4,mov,flv,3gp,3gpp',
	        'Url': '',
	]

	static Map tiposNotificacao = [
	        'BOLETOS_ATRASADO': 1,
			'SOCIO_DOCUMENTACAO_EXPIRAR': 2,
			'SOCIO_DOCUMENTACAO_PENDENTE': 3,
			'ARMA_DOCUMENTACAO_EXPIRAR': 4
	]

	static Map codigoMulta = [
			'1': 'Valor Fixo',
			'2': 'Percentual'
	];

	static Map prazoParaBaixa = [
			'0': 'Sem instrução',
			'1': 'Baixar em (*)',
			'3': 'Baixar após 365 dias do vencimento'
	];

	static Map codProtesto = [
			'0': 'Sem instrução',
			'1': 'Protestar (Dias Corridos) (*)',
			'2': 'Protestar (Dias Úteis) (*)',
			'3': 'Não protestar'
	];

	static Map codDesconto = [
			'1': 'Valor Fixo até a data informada',
			'2': 'Percentual até a data informada',
			'3': 'Valor Por Antecipação Dia Corrido',
			'4': 'Valor Por Antecipação Dia Útil',
			'5': 'Percentual sobre o Valor Nominal Dia Corrido',
			'6': 'Percentual sobre o Valor Nominal Dia Útil'
	];

	static Map codJuros = [
			'1': 'Valor por Dia',
			'2': 'Taxa Mensal',
			'3': 'Isento'
	];

	static Map bancoCodName = [
			'000': 'Caixa Administrativo',
			'001': 'Banco do Brasil S.A.',
			'033': 'Santander',
			'104': 'Caixa Econômica Federal',
			'237': 'Banco Bradesco S.A.',
			'341': 'Banco Itaú S.A.',
			'356': 'Banco Real',
			'399': 'HSBC Bank Brasil S.A.',
			'409': 'Unibanco'
	];

	static Map especieTitulo = [
			'1' : 'CHEQUE',
			'2' : 'DUPLICATA MERCANTIL',
			'3' : 'DUPLICATA MERCANTIL P/ INDICACAO',
			'4' : 'DUPLICATA DE SERVICO',
			'5' : 'DUPLICATA DE SERVICO P/ INDICACAO',
			'6' : 'DUPLICATA RURAL',
			'7' : 'LETRA DE CAMBIO',
			'8' : 'NOTA DE CREDITO COMERCIAL',
			'9' : 'NOTA DE CREDITO A EXPORTACAO',
			'10': 'NOTA DE CREDITO INDUSTRIAL',
			'11': 'NOTA DE CREDITO RURAL',
			'12': 'NOTA PROMISSORIA',
			'13': 'NOTA PROMISSORIA RURAL',
			'14': 'TRIPLICATA MERCANTIL',
			'15': 'TRIPLICATA DE SERVICO',
			'16': 'NOTA DE SEGURO',
			'17': 'RECIBO',
			'18': 'FATURA',
			'19': 'NOTA DE DEBITO',
			'20': 'APOLICE DE SEGURO',
			'21': 'MENSALIDADE ESCOLAR',
			'22': 'PARCELA DE CONSORCIO',
			'23': 'NOTA FISCAL',
			'24': 'DOCUMENTO DE DÍVIDA',
			'25': 'CÉDULA DE PRODUTO RURAL',
			'26': 'WARRANT',
			'27': 'DÍVIDA ATIVA DE ESTADO',
			'28': 'DÍVIDA ATIVA DE MUNICÍPIO',
			'29': 'DÍVIDA ATIVA DA UNIÃO',
			'30': 'ENCARGOS CONDOMINIAIS',
			'99': 'OUTROS'
	];
}
