package br.com.intersistemas.ireaderweb

/**
 * Created by solino on 26/09/2016.
 */
class RespostaWS {
    /**

     Codigo	    Significado ( a descricao fica a cargo do programador)
     100		Operacao realizada com sucesso
     101		Empresa nao encontrada
     102        Erro ao salvar usuario ( FuncionarioDigital)
     103 	    Operacao nao cadastrada
     104 	    Usuario ( FuncionarioDigital) nao encontrado

     106		Nao foi encontrado usuario com atributo template cadastrado
     107
     108		Erro ao salvar o registro da identificacao ( RegistroIdentificacao)

     */

    Integer codigo
    String descricao
    List<Object> lista
}
