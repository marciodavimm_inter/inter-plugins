package br.com.intersistemas.jasaas

/**
 * Created by MarcioDavi on 27/12/2016.
 */
interface IEmpresaAsaas {

    Long getId()
    void setId(Long id)

    String getTokenAsaas()
    void setTokenAsaas(String tokenAsaas)

    String getHomoTokenAsaas()
    void setHomoTokenAsaas(String homoTokenAsaas)

    Integer getAmbienteAsaas()
    void setAmbienteAsaas(Integer ambienteAsaas)

    Integer getDiasLembreteCobranca()
    void setDiasLembreteCobranca(Integer diasLembreteCobranca)

}