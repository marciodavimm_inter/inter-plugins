package br.com.intersistemas.jasaas

/**
 * Created by MarcioDavi on 27/12/2016.
 */
interface IBoletoAsaas {

    Long getId()
    void setId(Long id)

    IClienteAsaas getClienteAsaas()
    void setClienteAsaas(IClienteAsaas clienteAsaas)

    BigDecimal getValor()
    void setValor(BigDecimal valor)

    Date getDataVencimento()
    void setDataVencimento(Date dataVencimento)

    String getDescricaoParaBoleto()
    void setDescricaoParaBoleto(String descricaoParaBoleto)

    Long getNossoNumero()
    void setNossoNumero(Long nossoNumero)

    String getStatus()
    void setStatus(String status)


    String getIdAsaas()
    void setIdAsaas(String idAsaas)

    Boolean getDeletadoAsaas()
    void setDeletadoAsaas(Boolean deletadoAsaas)

    String getInvoiceUrl()
    void setInvoiceUrl(String invoiceUrl)

    String getBoletoUrl()
    void setBoletoUrl(String boletoUrl)

    String externalReference()
/*
    String externalReference() {
        return "bol_${id}"
    }
*/


}