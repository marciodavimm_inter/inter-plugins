package br.com.intersistemas.jasaas

/**
 * Created by MarcioDavi on 27/12/2016.
 */
interface IClienteAsaas {

    Long getId()
    void setId(Long id)

    IEmpresaAsaas getEmpresaAsaas()
    void setEmpresaAsaas(IEmpresaAsaas empresaAsaas)

    String getIdAsaas()
    void setIdAsaas(String idAsaas)

    String getRazaoSocial()
    void setRazaoSocial(String razaoSocial)

    String getNome()
    void setNome(String nome)

    String getEmail()
    void setEmail(String email)

    String getCnpj()
    void setCnpj(String cnpj)

    String getCpf()
    void setCpf(String cpf)


    String getCelular()
    void setCelular(String celular)

    String getTelefone()
    void setTelefone(String telefone)


    String getLogradouro()
    void setLogradouro(String logradouro)

    String getNumero()
    void setNumero(String numero)

    String getComplemento()
    void setComplemento(String complemento)

    String getBairro()
    void setBairro(String bairro)

    String getCodigoCidade()
    void setCodigoCidade(String codigoCidade)

    String getEstado()
    void setEstado(String estado)

    String getCep()
    void setCep(String cep)


    String externalReference()

/*
    String externalReference() {
        return "cli_${id}"
    }
*/

}